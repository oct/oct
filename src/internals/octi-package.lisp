;;;; -*- Mode: lisp -*-

;; If you want all core functions to be inline (like the C++ code
;; does), add :qd-inline to *features* by enabling the following line.
;; This makes compilation much, much slower, but the resulting code
;; conses much less and is significantly faster.
#+(not (and cmu x86))
(eval-when (:load-toplevel :compile-toplevel :execute)
  (pushnew :qd-inline *features*))

;; To be able to inline all the functions, we need to make
;; *inline-expansion-limit* much larger.
;;
;; Not sure we really want to inline everything, but the QD C++ code
;; inlines all of the functions so we do the same.  This makes CMUCL
;; take a very long time to compile the code, and the resulting
;; functions are huge.  (I think div-qd is 8 KB, and sqrt-qd is a
;; whopping 30 KB!)
;;
#+(and cmu qd-inline)
(eval-when (:load-toplevel :compile-toplevel :execute)
  (setf ext:*inline-expansion-limit* 1600))

;;
;; For all Lisps other than CMUCL, oct uses arrays to store the
;; quad-double values.  This is denoted by the feature :oct-array.
;; For CMUCL, quad-doubles can be stored in a (complex
;; double-double-float) object, which is an extension in CMUCL.
;; If you want CMUCL to use an array too, add :oct-array to *features*.
#-cmu
(pushnew :oct-array *features*)

(defpackage #:oct-internal
  (:use #:cl)
  (:nicknames #:octi)
  (:export #:%quad-double
	   #:read-qd
	   #:add-qd
	   #:add-qd-d
	   #:add-d-qd
	   #:sub-qd
	   #:sub-qd-d
	   #:sub-d-qd
	   #:neg-qd
	   #:mul-qd
	   #:mul-qd-d
	   #:sqr-qd
	   #:div-qd
	   #:div-qd-d
	   #:make-qd-d
	   #:integer-decode-qd
	   #:npow
	   #:qd-0
	   #:qd-1
	   #:qd-2
	   #:qd-3
	   #:qd-parts
	   #:+qd-one+
	   #:+qd-zero+
	   #:+qd-pi+
	   #:+qd-pi/2+
	   #:+qd-pi/4+
	   #:+qd-2pi+
	   #:+qd-log2+
	   ;; Functions
	   #:hypot-qd
	   #:abs-qd
	   #:sqrt-qd
	   #:log-qd
	   #:log1p-qd
	   #:exp-qd
	   #:sin-qd
	   #:cos-qd
	   #:tan-qd
	   #:sincos-qd
	   #:asin-qd
	   #:acos-qd
	   #:atan-qd
	   #:atan2-qd
	   #:sinh-qd
	   #:cosh-qd
	   #:tanh-qd
	   #:asinh-qd
	   #:acosh-qd
	   #:atanh-qd
	   #:qd-=
	   #:qd->
	   #:qd-<
	   #:qd->=
	   #:qd-<=
	   #:zerop-qd
	   #:plusp-qd
	   #:minusp-qd
	   #:integer-decode-qd
	   #:decode-float-qd
	   #:scale-float-qd
	   #:ffloor-qd
	   #:random-qd
	   #:with-qd-parts
	   #:rational-to-qd
	   )
  #+cmu
  (:export #:add-qd-dd
	   #:sub-qd-dd
	   #:div-qd-dd
	   #:make-qd-dd)
  #+cmu
  (:import-from #:c
		#:two-prod
		#:two-sqr)
  #+cmu
  (:import-from #:ext
		#:float-infinity-p
		#:float-nan-p
		#:float-trapping-nan-p
		#:double-double-float))
