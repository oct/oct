(require :asdf)
;; Need quicklisp so we can get the latest named-readtable
(unless (probe-file (merge-pathnames "quicklisp/" (user-homedir-pathname)))
  (and (load "quicklisp.lisp" :if-does-not-exist nil)
       (funcall (find-symbol "INSTALL" "QUICKLISP-QUICKSTART"))))
#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp" (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(ql:quickload :named-readtables)
(require :named-readtables)

(pushnew "./" asdf:*central-registry*)

(dribble "test.log")
(asdf:test-system :oct :force t)

