;;;; -*- Mode: lisp -*-

(defpackage #:octi-system
  (:use #:cl #:asdf))

(in-package #:octi-system)

(asdf:defsystem octi
    :description "Internals for oct"
  :author "Raymond Toy"
  :licence "MIT"
  :version "2020.11.27"
  :components
  ((:module "src/internals"
    :components
    ((:file "octi-package")
     (:file "qd-const"
      :depends-on ("qd-io")
      :around-compile (lambda (thunk)
			;; Just byte-compile these on cmucl since these are just constants
			(let (#+nil (ext:*byte-compile-default* t))
			  (funcall thunk))))
     #-cmu
     (:file "qd-dd" :depends-on ("octi-package" "qd-rep"))

     (:file "qd-fun"
      :depends-on ("qd" "qd-const"))
     (:file "qd-io"
      :depends-on ("qd"))
     (:file "qd"
      :depends-on ("qd-rep" #-cmu "qd-dd"))
     (:file "qd-rep"
      :depends-on ("octi-package"))))))
