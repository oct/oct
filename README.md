Oct,  A Lisp Implementation of Quad-Double Float

[[_TOC_]]

## Introduction
Oct is a portable Lisp implementation of quad-double arithmetic. This
gives about 65 digits of precision. Quad-double arithmetic uses four
double-float numbers to represent an extended precision number.

The implementation is modeled on the [quad-double
package](http://www.cs.berkeley.edu/~yozo/papers/LBNL-46996.ps.gz) by
[Yozo Hida](http://www.cs.berkeley.edu/~yozo/). This package is in
C++, but we have translated parts of it and extended it to use
Lisp. The intent is to provide all of the CL arithmetic functions with
a quad-double implementation.

Further information will be provided at a later date. This is
currently a work in progress, but the current code has the basic
functionality implemented and includes all of the special functions
specified by CL. There are, undoubtedly, many bugs.

## Supported Lisps
This package has been tested with CMUCL, SBCL, Clisp, and Allegro. All
tests pass. Note that development is primarily done using CMUCL.

See the [wiki](https://gitlab.common-lisp.net/oct/oct/-/wikis/home) for more information.

## Building Oct

1.  Obtain a copy of Oct:
    - For a fresh checkout:
      - Users:
      ```
      git clone https://gitlab.common-lisp.net/oct/oct.git
      ```
      - Developrs:
      ```
      git clone git@common-lisp.net:oct/oct.git
      ```
    - To update a copy:
      ```
      git pull
      ```
1.  Make sure you have asdf.  This is now commonly available with all
    supported lisps, but if not, use
    [Quicklisp](https://www.quicklisp.org/beta/) to get it.
1.  To compile the package, assuming you've started lisp in the same
    directory as oct:
    ```
    (pushnew "./" asdf:*central-registry*)
    (asdf:oos 'asdf:load-op :oct)
    ```
1.  If desired, you can also run the test suite:
    ```
    (asdf:test-system :oct)
    ```
    This should pass with some known test failures.

## Using Oct

Everything in Oct resides in the `NET.COMMON-LISP.OCT` package, with a
nickname of `OCT`. The basic arithmetic operations of CL are shadowed
in this package so you can use natural Lisp expressions to operate on
quad-doubles. Special functions are included.

There are two types added:

*  `QD-REAL`

    A quad-double type. This has about 65 digits of precision, or about 212 bits.
*  `QD-COMPLEX`

    A complex type consisting of two `QD-REAL` values.


The reader is also modified to make it easier to enter quad-double
values. `#q` is used to enter both `QD-REAL` and `QD-COMPLEX`
types. For example `#q1.25q5` is the `QD-REAL` with the value
125000. The exponent marker is `q`. To enter a `QD-COMPLEX` value,
use `#q(r i)` where `r` and `i` are the real and imaginary parts. The
parts will be coerced to `QD-REAL` type if necessary.

## Examples
Here are a few examples:
```
  * (in-package "OCT")
  * (named-readtables:in-readtable oct:oct-readtable)
  OCT> (/ (sqrt #q3) 2)
  #q0.866025403784438646763723170752936183471402626905190314027903489696q0
  OCT> (sin (/ +pi+ 3))
  #q0.86602540378443864676372317075293618347140262690519031402790348972q0
  OCT> (sqrt #q-1)
  #q(#q0.0q0 #q1.0q0)
  OCT> (coerce 7 'qd-complex)
  #q(#q7.0q0 #q0.0q0)
  OCT> (integer-decode-float +pi+)
  165424160919322423196824703508232170249081435635340508251270944637
  -215
  1
  OCT> +pi+
  #q3.1415926535897932384626433832795028841971693993751058209749445923q0
  OCT> (* 4 (atan #q1))
  #q3.1415926535897932384626433832795028841971693993751058209749445923q0
```
 
Note that `+pi+` is the `QD-REAL` value for pi.

## API
Oct has implementations for all of the numeric functions in
COMMON-LISP updated to support quad-doubles.  In addition, Oct
supports additional special functions

### Utilities

- [function] **`MAKE-QD`** _`X`_

  Creates a quad-double from `X` where `X` can be a rational, float, or a quad-double.

### Gamma

- [function] **`GAMMA`** _`Z`_

  Computes the gamma function [defined by](https://dlmf.nist.gov/5.2.E1)
  ```math
  \Gamma(z) = \int_0^{\infty} t^{z-1} e^{-t} dt
  ```
  for complex `Z`.
- [function] **`LOG-GAMMA`** _`Z`_

  Computes $`\log \Gamma(z)`$
- [function] **`PSI`** _`Z`_

  Computes the logarithmic derivative of $`\Gamma(z)`$ [defined by](https://dlmf.nist.gov/5.2.E2)
  ```math
  \psi(z) = \frac{\Gamma'(z)}{\Gamma(z)}
  ```

### Exponential, Logarithmic, Sine, and Cosine Integrals
- [function] **`SIN-INTEGRAL`** _`Z`_

   Computes the sin integral [defined by](https://dlmf.nist.gov/6.2.E9)
   ```math
   \mathrm{Si}(z) = \int_0^z \frac{\sin t}{t} dt
   ```
- [function] **`COS-INTEGRAL`** _`Z`_

   Computes the cosine integral [defined by](https://dlmf.nist.gov/6.2.E13)
   ```math
   \mathrm{Ci}(z) = \int_0^z \frac{1 - \cos t}{t} dt + \log z + \gamma
   ```


### Error Functions, Dawson's and Fresnel Integrals
- [function] **`ERF`** _`Z`_

  Computes the error function [defined by](https://dlmf.nist.gov/7.2.E1)
  ```math
  \mathrm{erf}\, z = \frac{2}{\sqrt\pi} \int_0^z e^{-t^2} dt
  ```
- [function] **`ERFC`** _`Z`_

   Computes the complementary error function [defined by](https://dlmf.nist.gov/7.2.E2):
   ```math
   \mathrm{erfc}\, z = \frac{2}{\sqrt\pi} \int_z^{\infty} e^{-t^2} dt = 1 - \mathrm{erf}\, z
   ```
- [function] **`FRESNEL-C`** _`Z`_

   Computes the Fresnel integral $`C(z)`$ [defined by](https://dlmf.nist.gov/7.2.E7):
   ```math
   C(z) = \int_0^z \cos\frac{\pi t^2}{2} dt
   ```
- [function] **`FRESNEL-S`** _`Z`_

  Computes the Fresnel integral $`S(z)`$ [defined by](https://dlmf.nist.gov/7.2.E8):
  ```math
  S(z) = \int_0^z \sin\frac{\pi t^2}{2} dt
  ```

### Incomplete Gamma and Related Functions
- [function] **`INCOMPLETE-GAMMA-LOWER`** _`A`_ _`Z`_ _`&KEY`_ _`NORMALIZED-P`_

   If `normalized-p` is `NIL`, return the [value](https://dlmf.nist.gov/8.2.E1):
   ```math
   \gamma(a, z) = \int_0^z t^{a-1} e^{-t} dt
   ```
   Otherwise return the normalized [value](https://dlmf.nist.gov/8.2.E4):
   ```math
   P(a, z) = \frac{1}{\Gamma(a)} \int_0^z t^{a-1} e^{-t} dt
   ```
- [function] **`INCOMPLETE-GAMMA-UPPER`** _`A`_ _`Z`_

   If `normalized-p` is `NIL`, return the [value](https://dlmf.nist.gov/8.2.E2):
   ```math
   \Gamma(a, z) = \int_z^{\infty} t^{a-1} e^{-t} dt
   ```
   Otherwise return the normalized [value](https://dlmf.nist.gov/8.2.E4):
   ```math
   Q(a, z) = \frac{1}{\Gamma(a)} \int_z^{\infty} t^{a-1} e^{-t} dt
   ```
- [function] **`INCOMPLETE-GAMMA-LOWER-NORMALIZED`** _`A`_ _`Z`_

   Returns the [value](https://dlmf.nist.gov/8.2.E4):
   ```math
   P(a, z) = \frac{1}{\Gamma(a)} \int_0^z t^{a-1} e^{-t} dt
   ```
- [function] **`INCOMPLETE-GAMMA-UPPER-NORMALIZED`** _`A`_ _`Z`_

   Returns the [value](https://dlmf.nist.gov/8.2.E4):
   ```math
   Q(a, z) = \frac{1}{\Gamma(a)} \int_z^{\infty} t^{a-1} e^{-t} dt
   ```  

### Elliptic Integrals

The elliptic integrals are defined in terms of $`m`$ where $`m = k^2`$
where $`k`$ is the [modulus](https://dlmf.nist.gov/19.1).

- [function] **`ELLIPTIC-K`** _`M`_

  Computes the complete elliptic integral of the first kind [defined
  by](https://dlmf.nist.gov/19.2.E8):
  ```math
  K(m) = F\left(\frac{\pi}{2}\biggr\rvert m\right) = \int_0^{\frac{\pi}{2}} \frac{dt}{\sqrt{1-m\sin^2 t}}
  ```
- [function] **`ELLIPTIC-F`** _`X`_ _`M`_

  Computes the elliptic integral of the first kind [defined by](https://dlmf.nist.gov/19.2.E4):
  ```math
  F(x|m) = \int_0^m \frac{dt}{\sqrt{1-m\sin^2 t}} = \int_0^{\sin x} \frac{dt}{\sqrt{1-t^2}\sqrt{1-mt^2}}
  ```
- [function] **`ELLIPTIC-E`** _`X`_ _`M`_

  Computes the elliptic integral of the second kind [defined by](https://dlmf.nist.gov/19.2.E5):
  ```math
  E(x|m) = \int_0^x \sqrt{1-m\sin^2 t}\, dt = \int_0^{\sin x} \frac{\sqrt{1-mt^2}}{\sqrt{1-t^2}}\, dt
  ```
- [function] **`ELLIPTIC-EC`** _`X`_ _`M`_

  Computes the complete elliptic integral of the second kind [defined by](https://dlmf.nist.gov/19.2.E8)
  ```math
  E(m) = E\left(\frac{\pi}{2}\biggr\rvert m\right) = \int_0^{\frac{\pi}{2}} \sqrt{1-m\sin^2 t}\, dt
  ```
- [function] **`CARLSON-RD`** _`X`_ _`Y`_ _`Z`_ 

   Computes Carlson's integral $`R_D`$ [defined by](https://dlmf.nist.gov/19.16.E5):
   ```math
   R_D(x,y,z) = \frac{3}{2} \int_0^\infty \frac{dt}{(t+z)\sqrt{t+x}\sqrt{t+y}\sqrt{t+z}}
   ```
- [function] **`CARLSON-RF`** _`X`_ _`Y`_ _`Z`_ 

   Computes Carlson's integral $`R_F`$ [defined by](https://dlmf.nist.gov/19.16.E1):
   ```math
   R_F(x,y,z) = \frac{1}{2} \int_0^\infty \frac{dt}{\sqrt{t+x}\sqrt{t+y}\sqrt{t+z}}
   ```
- [function] **`CARLSON-RJ`** _`X`_ _`Y`_ _`Z`_ _`P`_

  Computes Carlson's integral $`R_J`$ [defined by](https://dlmf.nist.gov/19.16.E2)
  ```math
  R_J(x,y,z,p) = \frac{3}{2} \int_0^\infty \frac{dt}{(t+p)\sqrt{t+x}\sqrt{t+y}\sqrt{t+z}}
  ```
### Theta Functions

The theta functions can be defined in terms of the parameter $`\tau`$
but the definitions used here use the nome $`q`$ where $`q =
e^{i\pi\tau}`$.

- [function] **`ELLIPTIC-THETA`** _`N`_ _`Z`_ _`Q`_

  Computes the elliptic theta function for $`N = 1,2,3,4`$.
- [function] **`ELLIPTIC-THETA-1`** _`Z`_ _`Q`_

  Computes the first theta function as a function of the nome $`q`$ [defined by](https://dlmf.nist.gov/20.2.E1):
  ```math
  \theta_1(z, q) = 2 \sum_{n=0}^{\infty} \left(-1\right)^n q^{\left(n+\frac{1}{2}\right)^2} \sin (2n+1)z
  ```
- [function] **`ELLIPTIC-THETA-2`** _`Z`_ _`Q`_

  Computes the second theta function as a function of the nome $`q`$ [defined by](https://dlmf.nist.gov/20.2.E2):
  ```math
  \theta_2(z, q) = 2 \sum_{n=0}^{\infty} q^{\left(n+\frac{1}{2}\right)^2} \cos (2n+1)z
  ```
- [function] **`ELLIPTIC-THETA-3`** _`Z`_ _`Q`_

  Computes the first theta function as a function of the nome $`q`$ [defined by](https://dlmf.nist.gov/20.2.E3):
  ```math
  \theta_3(z, q) = 1 + 2 \sum_{n=0}^{\infty} q^{n^2} \cos 2nz
  ```
- [function] **`ELLIPTIC-THETA-4`** _`Z`_ _`Q`_

  Computes the fourth theta function as a function of the nome $`q`$ [defined by](https://dlmf.nist.gov/20.2.E4):
  ```math
  \theta_4(z, q) = 1 + 2 \sum_{n=0}^{\infty} \left(-1\right)^n q^{n^2} \cos 2nz
  ```

### Jacobian Elliptic Functions
For the elliptic functions, [define the _nome_](https://dlmf.nist.gov/22.2.E1) $`q`$ in terms of the parameter $`m`$ by:
```math
q = \mathrm{exp}\left(\frac{-\pi K'(m)}{K(m)}\right)
```
Also [define](https://dlmf.nist.gov/22.2.E3)
```math
\zeta = \frac{\pi u}{2K(m)}
```
- [function] **`JACOBI-SN`** _`U`_ _`M`_

  Computes the Jacobi $`\mathrm{sn}`$ function [defined by](https://dlmf.nist.gov/22.2.E4):
  ```math
  \mathrm{sn}(u|m) = \frac{\theta_3(0,q)}{\theta_2(0,q)}\frac{\theta_1(\zeta, q)}{\theta_4(\zeta, q)}
  ```
- [function] **`JACOBI-CN`** _`U`_ _`M`_

  Computes the Jacobi $`\mathrm{cn}`$ function [defined by](https://dlmf.nist.gov/22.2.E5):
  ```math
  \mathrm{cn}(u|m) = \frac{\theta_4(0,q)}{\theta_2(0,q)}\frac{\theta_2(\zeta, q)}{\theta_4(\zeta, q)}
  ```
- [function] **`jacobi-dn`**  _`U`_ _`M`_

  Computes the Jacobi $`\mathrm{dn}`$ function [defined by](https://dlmf.nist.gov/22.2.E6):
  ```math
  \mathrm{dn}(u|m) = \frac{\theta_4(0,q)}{\theta_3(0,q)}\frac{\theta_3(\zeta, q)}{\theta_4(\zeta, q)}
  ```
